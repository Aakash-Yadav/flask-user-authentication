
from cryptography.fernet import  Fernet

with open('key.txt','rb') as f:
    data = f.read()
f = Fernet(data)

def convert_into_encrypt(n:str):
    n = n.encode()
    return f.encrypt(n)

def convert_to_decrypt(n):
    value = (f.decrypt(n))
    return ''.join(map(chr, value))


    










