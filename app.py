from flask import (Flask , session , render_template , redirect,url_for )
from os import getcwd,urandom,path
from flask_sqlalchemy import SQLAlchemy
from flask_login import (UserMixin,login_user,LoginManager,login_required,logout_user)
from flask_wtf import FlaskForm
from wtforms import (StringField,PasswordField,SubmitField)
from wtforms.validators import InputRequired,Length,ValidationError
from encryption import convert_into_encrypt,convert_to_decrypt

app = Flask(__name__)

#########################
####### APP CONFIG ######
#########################
app.config['SECRET_KEY'] = '%s-1-%s-2-%s'%(urandom(50),urandom(50),urandom(50))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+path.join(getcwd(),'test.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 0 

db = SQLAlchemy(app)

##########################
####### LoginManager #####
##########################

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'LOGIN'


@login_manager.user_loader
def load_user(user_id):
    print(user_id)
    return User.query.get(int(user_id))

############################
#### DATA-ORM #############
###########################
class User(db.Model,UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    Password = db.Column(db.Text,nullable=False)
##########################
####### WTF FORM #########
##########################

""" REGISTER PAGE """
class USER_FORM(FlaskForm):
    user_name = StringField(validators=[InputRequired(),Length(min=4,max=18),],render_kw={'placeholder':'UserName'})
    Password = PasswordField(validators=[InputRequired(),Length(min=4,max=16),],render_kw={'placeholder':'Password'})
    submit = SubmitField('Regiser')
    
    def user_name_validation(self,user_name):
        exist_user_name = User.query.filter_by(username=user_name).first()
        if exist_user_name:
            raise ValidationError('USER NAME ALREADY TAKEN ')
        
""" LOGIN PAGE """
class LOGIN_FORM(FlaskForm):
    user_name = StringField(validators=[InputRequired(),Length(min=4,max=18),],render_kw={'placeholder':'UserName'})
    Password = PasswordField(validators=[InputRequired(),Length(min=4,max=16),],render_kw={'placeholder':'Password'})
    submit = SubmitField('Login')
        
###################### 
####### MAIN APP #####
######################

#######################################
############### HOME ##################
#######################################
@app.route('/')
def Home():
    return render_template('home.html')
#######################################
############### LOGIN #################
#######################################

@app.route('/login',methods=['POST','GET'])
def LOGIN():
    login_form = LOGIN_FORM()
    if login_form.validate_on_submit():
        form_data_user_name  = login_form.user_name.data
        form_data_user_password = login_form.Password.data
        IDK = User.query.filter_by(username=form_data_user_name).first()
        
        if IDK:
            if (convert_to_decrypt(IDK.Password)==form_data_user_password):
                session['NAME']= IDK.username
                login_user(IDK)
              
                return redirect(url_for('dashbord'))
    return render_template('login.html',login_form=login_form)

#######################################
############### Dashbord ##############
#######################################

@app.route('/Dashbord',methods=['POST','GET'])
@login_required
def dashbord():
    name =  session['NAME']
    return render_template('dashboard.html',name=name)

#######################################
############### LOGOUT ################
#######################################
@app.route('/logout',methods=['POST','GET'])
@login_required
def Logout():
    logout_user()
    return redirect(url_for('Home'))

#######################################
############### Register ##############
#######################################
@app.route('/register',methods=['POST','GET'])
def Registerx():
    Register = USER_FORM()
    if Register.validate_on_submit():
        session['USER_NAME'] = Register.user_name.data
        Password_ = convert_into_encrypt(Register.Password.data)
        new = User(username=session['USER_NAME'],Password=Password_)
        db.session.add(new)
        db.session.commit()
        return redirect(url_for('LOGIN'))
    return render_template('register.html',Register_form = Register)

if __name__ == '__main__':
    app.run(debug=1)