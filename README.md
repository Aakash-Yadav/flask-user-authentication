#### Flask User Authentication
![alt text](https://xp.io/storage/2n6kwhY1.png)
#####  Flask user authentication using the Flask-Login Library and SQLAlchemy.
***Nowadays, almost all the websites have user authentication systems coded into them. We can set-up an account with user authentication either directly or via a third-party like Google, Facebook. Apple etc.***


## Hands-On with Flask User Authentication
Flask-login uses Cookie-based Authentication. When the client logins via his credentials, Flask creates a session containing the user ID and then sends the session ID to the user via a cookie, using which he can log in and out as and when required.

##### First we need to install the requirements module
1. pip3 install Flask
2. pip3 install Flask-login
3. pip3 install Flask-sqlalchemy
4. pip3 install Flask-wtf`

### Project Overview :
Authentication might not be necessary for simple presentation sites but mandatory for other types of projects where the project design requires to know when a user is authenticated and what resources to be accessed based on his credentials (username, profiles .. etc). The project we will code provides a simple codebase structure, SQLite persistence, and three pages (index, login, register) styled with Bootstrap 5. Here are the project dependencies:

###  What is Flask :/
Flask is a popular Python Framework designed to a project quick and easy, with the ability to scale up to complex applications. Flask can be used to code from simple one-page sites to APIs and complex eCommerce solutions.

### Flask-Login Library
Flask-Login, probably the most popular authentication library for Flask, provides user session management and handles the common tasks of logging in, logging out, and remembering your users’ sessions over extended periods of time.
#### How it Works
- ## Home Page:
![alt text](https://xp.io/storage/2n72M2Ka.png)
##### It Just container Of Two Button Login and Register 
- ## Register Page:
![alt text](https://xp.io/storage/2n7e2RDP.png)
###### Two Input 
1. USER-NAME
2. PASSWORD
##### features-: Password encryption
![alt text](https://xp.io/storage/2n7okR84.png)

- ## Login Page :
![alt text](https://xp.io/storage/2n7xfC97.png)
1. USER-NAME
2. PASSWORD
##### features-: Password decrypt
![alt text](https://xp.io/storage/2n7IPvQg.png)

- ## Dashboard 
#### After Login Successful
![alt text](https://xp.io/storage/2n7Vf3Br.png)
- ## Logout
###### Redirect To Home Page
![alt text](https://xp.io/storage/2n72M2Ka.png)


#### thank you







